library("frenchLandscape")
expectation <- 0
for(year in 2007:2014){
    cat("\n",year,":")
    for(dep in c(1:19,21:95)){
        ilotsCult <- LoadDepartements(dep,baseName="RPG",year=year,layerName="ilotsCult")
        # res <- length(which(is.na(ilotsCult$coordsAsString)))
        # if(res != expectation) cat(dep,res,";")
        ilotsCult$RATIO_ILOT <- ilotsCult$SURFACE_GROUPE_CULTURE/ilotsCult$SURFACE_ILOT
        varName <- paste0("ilotsCult_",year,"_",NumTo3Char(dep))
        saveAs(variable=ilotsCult,name=varName,outDir=file.path(GetDicoPath("RPG"),"V2"))
    }
}

### avant d'ajouter -skipfailures (13 juin 2017), pas appliqué à tous les départements ceci dit
### voir /media/5AE1EC8814E5040E/carto_init/RPG/2009/039/README
# investigate weird number of duplicates
old <- get(load(file.path(GetDicoPath("RPG"),"ilotsCult_2009_038.rda")))
new <- get(load(file.path(GetDicoPath("RPG"),"V2/ilotsCult_2009_038.rda")))
rm(ilotsCult_2009_038)
library("spatDataManagement")
ilotsCult <- old
surf_cultures <- aggregate(ilotsCult$SURFACE_GROUPE_CULTURE,
                           by=list(culture=ilotsCult$CODE_GROUPE_CULTURE,
                                   coordsAsString=ilotsCult$coordsAsString),sum)
surf_cultures <- ChangeNameCol(surf_cultures,"x","surfCult")

IDRem <- setdiff(ilotsCult$coordsAsString,surf_cultures$coordsAsString)
length(IDRem)
#=> only 5 duplicates, normal

dim(surf_cultures)
# 125600
dim(ilotsCult)
# 140026
#=> perte de 10% des lignes !?!

# pb : les lignes qui sont NA pour coordsAsString:
length(which(is.na(ilotsCult$coordsAsString)))
# 14406
# à 20 près ce qu'il manque à ilotsCult

summary(ilotsCult[which(is.na(ilotsCult$coordsAsString)),])
summary(ilotsCult[which(!is.na(ilotsCult$coordsAsString)),])
#=> ça a l'air très similaire

# est-ce que ilots manque de lignes dans ilotsCult ?
numDep <- 39
year <- 2009
ilots <- LoadDepartements(numDep,baseName="RPG",year=year,layerName="ilots")
length(which(is.na(match(ilots$ID_ILOT,ilotsCult$ID_ILOT))))
#=> aucun problème

# par contre évidemment manque tout dans l'autre sens
length(which(is.na(match(ilotsCult$ID_ILOT,ilots$ID_ILOT))))
# 14406

# comment la somme des surfaces se compare-t-elle à la SAU du département les autres années?
library("rgeos")
ilotsSuiv <- LoadDepartements(numDep,baseName="RPG",year=year+1,layerName="ilots")
ilotsPrec <- LoadDepartements(numDep,baseName="RPG",year=year-1,layerName="ilots")
aCur <- gArea(ilots)
aPrec <- gArea(ilotsPrec)
aSuiv <- gArea(ilotsSuiv)
cat("%diff to prec:",(aCur-aPrec)/aPrec,"\n")
#=> missing 21.6%
cat("%diff to suiv:",(aCur-aSuiv)/aSuiv,"\n")
#=> missing 25.0%

# also seen on the number of polygons
NRows(ilots)-NRows(ilotsPrec)
# -12356, similar to what is missing here

#=> the departement is really missing an important share of its polygons

# is it localized ?
par(mfrow=c(1,3))
plot(coordinates(ilotsPrec),pch=".")
plot(frenchDepartements,add=T,border="green")
plot(coordinates(ilots),pch=".")
plot(frenchDepartements,add=T,border="green")
plot(coordinates(ilotsSuiv),pch=".")
plot(frenchDepartements,add=T,border="green")
#38 => suiv has many out of departement polygons in the south
#   otherwise looks quite similar
#   on voit quand même des manques clairs dans le sud-est

#39=> the catastrophe is obvious but fairly well spread 


## check the surfaces in the new files
V2folder <- file.path(GetDicoPath("RPG"),"V2")
ilotsCult <- LoadDepartements(c(1:19,21:95),"RPG",2007,"ilotsCult",inFolder=V2folder)

# check the polygons that changed
iChange <- which(ilotsCult$SURFACE_ILOT!=ilotsCult$SURFACE_ILOT_init)
plot(ilotsCult$SURFACE_ILOT[iChange],ilotsCult$SURFACE_GROUPE_ILOT_init[iChange],pch=".")
plot(ilotsCult$SURFACE_ILOT[iChange],ilotsCult$surface_pol_ilot[iChange]/10^4,pch=3)
points(ilotsCult$SURFACE_ILOT_init[iChange],ilotsCult$surface_pol_ilot[iChange]/10^4,pch=3,col="green")
abline(a=0,b=1)

sRef <- ilotsCult$surface_pol_ilot[iChange]/10^4
devInit <- (ilotsCult$SURFACE_ILOT_init[iChange]-sRef)/sRef
devNew <- (ilotsCult$SURFACE_ILOT[iChange]-sRef)/sRef
table(abs(devInit)>abs(devNew)) # améliorations ?
#=> non, nettement

plot(devInit,devNew)
abline(a=0,b=1)
#=> devInit était en général faible et plutôt négatif: la somme des parcelles 
#   n'atteignait pas la taille du polygone
#   devNew a soit pas bougé soit vraiment agravé tout un paquet de points
#   c'est vraiment étrange
ic <- data.table(ilotsCult)

points(ilotsCult$SURFACE_ILOT_init[iChange],ilotsCult$surface_pol_ilot[iChange]/10^4,pch=3,col="green")

head(ilotsCult[iChange,])


